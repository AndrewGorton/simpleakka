#!/bin/bash

cp ../target/SimpleAkka-3.0.0-SNAPSHOT.jar .
docker build --pull=true --force-rm=true -t andrewgortonuk/simpleakka:3.0.0-snapshot .

if [ $? -eq 0 ]
then
    echo Try docker run -it andrewgortonuk/simpleakka:3.0.0-snapshot
else
    echo Build failure detected
    exit -1
fi

exit 0
