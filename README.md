# SimpleAkka

A simple Java-based [Akka](http://akka.io) application.

Version 1 shows how Akka passes messages remotely.

Version 2 demonstrates programmatic cluster creation and message passing between a master node and slave nodes.

## Compile

Uses Maven 3.3.1 and Oracle Java 1.8.0_20 to create a fat jar.

```
$ mvn package
```

### Running

Akka needs to know which address and port to bind to, and which address and port to advertise itself upon. I use
0.0.0.0 as the bind address, and 127.0.0.1 as the advertised address. If you get this wrong, you will end up with
messages being discarded ("dropping message for non-local receipient").

Run a master node first, which will send a sayHello message every 10 seconds to the slaves.

```
$ java -jar target/SimpleAkka-3.0.0-SNAPSHOT.jar master 0.0.0.0:2551 127.0.0.1:2551
New member: akka.tcp://SimpleAkka@127.0.0.1:2551 (1 total members)
New member: akka.tcp://SimpleAkka@127.0.0.1:2552 (2 total members)
New member: akka.tcp://SimpleAkka@127.0.0.1:2553 (3 total members)
master received 'sayHello' at 1444217663635
WorkResponse: akka.tcp://SimpleAkka@127.0.0.1:2552 start 1444217663637msec, end 1444217663738msec, duration 101msec
WorkResponse: akka.tcp://SimpleAkka@127.0.0.1:2553 start 1444217663637msec, end 1444217663738msec, duration 101msec
...
```

In another console window (to run a slave in a separate process) - this prints to the console when it receives a 
sayHello message from the master.

```
$ java -jar target/SimpleAkka-3.0.0-SNAPSHOT.jar slave 127.0.0.1:2551 0.0.0.0:2552 127.0.0.1:2552
Seed node: akka.tcp://SimpleAkka@127.0.0.1:2551
slave received 'WorkRequest' at 1444217663637
slave received 'WorkRequest' at 1444217673637
...
```

Press control-C to kill the instances. You can run as many slaves as you like - they'll automatically register with
the master and receive messages.

You can also kill the master and restart it, and the client(s) should reconnect
automatically.
