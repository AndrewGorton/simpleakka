package uk.andrewgorton.akka.simpleakkalocal.actors;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.cluster.UniqueAddress;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Master extends UntypedActor {
    private static final int slaveCount = 10;

    private List<ActorRef> slaves = null;
    private Cluster cluster = null;
    private Map<UniqueAddress, Member> nodes = new HashMap<UniqueAddress, Member>();

    //**************************************************
    // Messages which we expect to receive
    //**************************************************
    static public class SayHello implements Serializable {

    }

    static public class WorkResponse implements Serializable {
        private long start;
        private long end;

        public long getStart() {
            return start;
        }

        public void setStart(long start) {
            this.start = start;
        }

        public long getEnd() {
            return end;
        }

        public void setEnd(long end) {
            this.end = end;
        }

        public long getDuration() {
            return end - start;
        }
    }

    //**************************************************
    // Class logic
    //**************************************************

    public Master() {
        cluster = Cluster.get(getContext().system());
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();

        // Create the required number of local in-process slaves
        if (slaves == null) {
            slaves = new ArrayList<ActorRef>();
            for (int counter = 0; counter < slaveCount; ++counter) {
                //slaves.add(getContext().actorOf(Props.create(Slave.class), String.format("slave%d", counter)));
            }
        }

        // Listen for remote slaves joining and leaving
        cluster.subscribe(getSelf(),
                ClusterEvent.initialStateAsEvents(),
                ClusterEvent.MemberEvent.class,
                ClusterEvent.UnreachableMember.class);
    }

    @Override
    public void postStop() throws Exception {
        cluster.unsubscribe(getSelf());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ClusterEvent.MemberEvent || message instanceof ClusterEvent.ReachabilityEvent) {
            if (!handleClusterEvent(message)) {
                unhandled(message);
            }
        } else if (message instanceof SayHello) {
            sayHello();
        } else if (message instanceof WorkResponse) {
            handleWorkResponse((WorkResponse) message);
        } else {
            unhandled(message);
        }
    }

    private boolean handleClusterEvent(Object message) {
        boolean messageHandled = false;

        if (message instanceof ClusterEvent.MemberUp) {
            ClusterEvent.MemberUp m = (ClusterEvent.MemberUp) message;
            if (nodes.put(m.member().uniqueAddress(), m.member()) != null) {
                System.out.println(String.format("Warning: Replaced member '%s'", m.member().address()));
            }
            System.out.println(String.format("New member: %s (%d total members)", m.member().address(), nodes.size()));
            messageHandled = true;

        } else if (message instanceof ClusterEvent.UnreachableMember) {
            ClusterEvent.UnreachableMember um = (ClusterEvent.UnreachableMember) message;
            if (nodes.remove(um.member().uniqueAddress()) == null) {
                System.out.println(String.format("Warning: Member removed was unknown'%s'", um.member().address()));
            } else {
                System.out.println(String.format("Removed member: %s (%d total members)", um.member().address(), nodes.size()));
            }
            messageHandled = true;
        }
        return messageHandled;
    }

    private void sayHello() {
        System.out.println(String.format("%s received 'sayHello' at %d", getSelf().path().name(), System.currentTimeMillis()));

        // Propagate to our local slaves
        for (ActorRef singleSlave : slaves) {
            singleSlave.tell(new Slave.WorkRequest(), getSelf());
        }

        // Propagate to our remote slaves
        for (Map.Entry<UniqueAddress, Member> singleEntry : nodes.entrySet()) {
            if (singleEntry.getValue().hasRole("slave")) {
                ActorSelection as = getContext().actorSelection(singleEntry.getValue().address() + "/user/slave");
                as.tell(new Slave.WorkRequest(), getSelf());
            }
        }
    }

    private void handleWorkResponse(WorkResponse message) {
        System.out.println(String.format("WorkResponse: %s start %dmsec, end %dmsec, duration %dmsec",
                getSender().path().address(),
                message.getStart(),
                message.getEnd(),
                message.getDuration()));
    }
}
