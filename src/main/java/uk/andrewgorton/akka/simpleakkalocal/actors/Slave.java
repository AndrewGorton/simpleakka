package uk.andrewgorton.akka.simpleakkalocal.actors;

import akka.actor.UntypedActor;

import java.io.Serializable;
import java.util.concurrent.ExecutorService;

public class Slave extends UntypedActor {
    private ExecutorService es = null;

    //**************************************************
    // Messages which we expect to receive
    //**************************************************

    static public class WorkRequest implements Serializable {

    }

    static private class Timing {
        private long start;
        private long end;

        public long getStart() {
            return start;
        }

        public void setStart(long start) {
            this.start = start;
        }

        public long getEnd() {
            return end;
        }

        public void setEnd(long end) {
            this.end = end;
        }
    }

    //**************************************************
    // Class logic
    //**************************************************

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof WorkRequest) {
            Timing t = handleWorkRequest();
            Master.WorkResponse wr = new Master.WorkResponse();
            wr.setStart(t.getStart());
            wr.setEnd(t.getEnd());
            getSender().tell(wr, getSelf());
        } else {
            unhandled(message);
        }
    }

    private Timing handleWorkRequest() {
        Timing result = new Timing();
        result.setStart(System.currentTimeMillis());
        System.out.println(String.format("%s received 'WorkRequest' at %d", getSelf().path().name(), result.getStart()));
        
        // Simulate work
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            // Don't care
        }
        
        result.setEnd(System.currentTimeMillis());

        return result;
    }
}
