package uk.andrewgorton.akka.simpleakkalocal;

import akka.actor.*;
import akka.cluster.Cluster;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import scala.collection.JavaConversions;
import uk.andrewgorton.akka.simpleakkalocal.actors.Master;
import uk.andrewgorton.akka.simpleakkalocal.actors.Slave;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        if (args.length > 0) {
            if (args[0].compareToIgnoreCase("master") == 0) {
                new Main().runMaster(args[1], args[2]);
            } else if (args[0].compareToIgnoreCase("slave") == 0) {
                new Main().runSlave(args[1], args[2], args[3]);
            } else {
                showHelp();
            }
        } else {
            showHelp();
        }
    }

    public static void showHelp() {
        System.out.println("Usage:-");
        System.out.println("master <bindIp>:<bindPort> <exposedIp>:<exposedPort> - runs a master node (min 1 required)");
        System.out.println("slave <seedIp>:<seedPort> <bindIp>:<bindPort> <exposedIp>:<exposedPort> - runs a slave node");
    }

    public void runMaster(final String bindAddress, final String exposedAddress) {
        String[] bindParts = bindAddress.split(":");
        String[] exposedParts = exposedAddress.split(":");

        List<String> seeds = new ArrayList<String>();
        seeds.add(String.format("akka.tcp://SimpleAkka@%s:%s", exposedParts[0], exposedParts[1]));
        List<String> roles = new ArrayList<String>();
        roles.add("master");
        Config config = com.typesafe.config.ConfigFactory.empty()
                .withValue("akka.actor.provider", ConfigValueFactory.fromAnyRef("akka.cluster.ClusterActorRefProvider"))
                .withValue("akka.actor.serializers.java", ConfigValueFactory.fromAnyRef("akka.serialization.JavaSerializer"))
                .withValue("akka.remote.untrusted-mode", ConfigValueFactory.fromAnyRef("off"))
                .withValue("akka.remote.netty.tcp.bind-hostname", ConfigValueFactory.fromAnyRef(bindParts[0]))
                .withValue("akka.remote.netty.tcp.bind-port", ConfigValueFactory.fromAnyRef(bindParts[1]))
                .withValue("akka.remote.netty.tcp.hostname", ConfigValueFactory.fromAnyRef(exposedParts[0]))
                .withValue("akka.remote.netty.tcp.port", ConfigValueFactory.fromAnyRef(exposedParts[1]))
                .withValue("akka.cluster.seed-nodes", ConfigValueFactory.fromIterable(seeds))
                .withValue("akka.cluster.auto-down-unreachable-after", ConfigValueFactory.fromAnyRef("10s"))
                .withValue("akka.cluster.roles", ConfigValueFactory.fromIterable(roles))
                .withFallback(ConfigFactory.load());

        ActorSystem system = ActorSystem.create("SimpleAkka", config);
        ActorRef master = system.actorOf(Props.create(Master.class), "master");
        master.tell(new Master.SayHello(), ActorRef.noSender());

        // Every 10 seconds, send a sayHello message
        while (true) {
            try {
                Thread.sleep(10000);
            } 
            catch (InterruptedException e) {
                // Don't care
            }

            master.tell(new Master.SayHello(), ActorRef.noSender());
        }
    }

    public void runSlave(final String seedAddress, final String bindAddress, final String exposedAddress) {
        String[] seedParts = seedAddress.split(":");
        String[] bindParts = bindAddress.split(":");
        String[] exposedParts = exposedAddress.split(":");

        List<Address> seeds = new ArrayList<Address>();
        String seedNodeDetails = String.format("akka.tcp://SimpleAkka@%s:%s", seedParts[0], seedParts[1]);
        System.out.println("Seed node: " + seedNodeDetails);
        seeds.add(AddressFromURIString.parse(seedNodeDetails));

        List<String> roles = new ArrayList<String>();
        roles.add("slave");

        Config config = com.typesafe.config.ConfigFactory.empty()
                .withValue("akka.actor.provider", ConfigValueFactory.fromAnyRef("akka.cluster.ClusterActorRefProvider"))
                .withValue("akka.actor.serializers.java", ConfigValueFactory.fromAnyRef("akka.serialization.JavaSerializer"))
                .withValue("akka.remote.untrusted-mode", ConfigValueFactory.fromAnyRef("off"))
                .withValue("akka.remote.netty.tcp.bind-hostname", ConfigValueFactory.fromAnyRef(bindParts[0]))
                .withValue("akka.remote.netty.tcp.bind-port", ConfigValueFactory.fromAnyRef(bindParts[1]))
                .withValue("akka.remote.netty.tcp.hostname", ConfigValueFactory.fromAnyRef(exposedParts[0]))
                .withValue("akka.remote.netty.tcp.port", ConfigValueFactory.fromAnyRef(exposedParts[1]))
                .withValue("akka.cluster.auto-down-unreachable-after", ConfigValueFactory.fromAnyRef("10s"))
                .withValue("akka.cluster.roles", ConfigValueFactory.fromIterable(roles))
                .withFallback(ConfigFactory.load());

        ActorSystem system = ActorSystem.create("SimpleAkka", config);
        ActorRef slave = system.actorOf(Props.create(Slave.class), "slave");
        Cluster.get(system).joinSeedNodes(JavaConversions.asScalaBuffer(seeds).toList());
        system.awaitTermination();
    }
}
